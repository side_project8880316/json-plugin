package org.jsonplugin.jsonhoverplugin;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;

public class JSONStringAnnotator implements Annotator {
    @Override
    public void annotate(@NotNull final PsiElement element, @NotNull AnnotationHolder holder) {
        String value = element.getText();

        if (value != null && isJSON(value)) {
            holder.newSilentAnnotation(HighlightSeverity.INFORMATION)
                    .range(element)
                    .tooltip("JSON String: " + formatJSON(value))
                    .create();
        }
    }

    private boolean isJSON(String value) {
        return value.startsWith("{") && value.endsWith("}");
    }

    private String formatJSON(String value) {
        value = value.replace("\\", "\\\\").replace("\"", "\\\"");
        return value;
    }
}